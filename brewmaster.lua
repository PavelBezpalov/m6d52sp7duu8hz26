-- Brewmaster Monk for 8.1 by Nikopol - 12/2018
-- Talents: 
-- Left Control - Leg Sweep
-- Left Alt - Tiger's Lust
-- Left Shift - Vivify self
local dark_addon = dark_interface
local SB = m2jue4dgc56acfzz
local TG = torghast_spells

local function haste_mod()
  local haste = UnitSpellHaste("player")
  return 1 + haste / 100
end

local function gcd_duration()
  return 1.5 / haste_mod()
end

local function gcd()
  if not player.alive then return end
  
  if GetItemCooldown(5512) == 0 and player.health.effective < 30 then
    macro('/use Healthstone')
  end
  
  if castable(SB.HealingElixir) and player.health.effective < 30 then
    cast(SB.HealingElixir)
  end
  
  if castable(SB.FortBrew) and player.health.effective < 20 then
    cast(SB.FortBrew)
  end
  
  if castable(SB.BlackOxBrew) and spell(SB.PurifyingBrew).charges == 0 and spell(SB.CelestialBrew).cooldown > 0 then
    cast(SB.BlackOxBrew)
  end
  
  if castable(SB.PurifyingBrew)
    and player.buff(SB.CelestialBrew).down
    and (player.debuff(SB.HeavyStagger).up 
      or ((player.debuff(SB.LightStagger).up or player.debuff(SB.ModerateStagger).up) 
        and (spell(SB.PurifyingBrew).charges == 2 or spell(SB.PurifyingBrew).charges == 1 and spell(SB.PurifyingBrew).recharge < 2))) then
    cast(SB.PurifyingBrew)
  end
  
  if target.enemy and target.alive and target.distance <= 5 and toggle('interrupts', false) and target.interrupt(70) and spell(SB.SpearHandStrike).cooldown == 0 then
    cast(SB.SpearHandStrike, target)
  end
end

local function combat()
  if not player.alive then return end
  
  if GetCVar("nameplateShowEnemies") == '0' then
    SetCVar("nameplateShowEnemies", 1)
  end
  
  macro('/cqs')
  
  if modifier.lcontrol and castable(SB.LegSweep) then
    return cast(SB.LegSweep)
  end
  
  if modifier.lshift and castable(SB.Vivify) then
    return cast(SB.Vivify, player)
  end
  
  if modifier.lalt and castable(SB.TigersLust) then
    return cast(SB.TigersLust, player)
  end
  
  if GetItemCooldown(5512) == 0 and player.health.effective < 30 then
    macro('/use Healthstone')
  end
  
  --if GetItemCooldown(5512) > 0 and GetItemCooldown(177278) == 0 and player.health.effective < 20 then
  --  macro('/use Phial of Serenity')
  --end
  
  if castable(SB.HealingElixir) and player.health.effective < 30 then
    cast(SB.HealingElixir)
  end
  
  if castable(SB.FortBrew) and player.health.effective < 20 then
    cast(SB.FortBrew)
  end
  
  if castable(SB.BlackOxBrew) and spell(SB.PurifyingBrew).charges == 0 and spell(SB.CelestialBrew).cooldown > 0 then
    cast(SB.BlackOxBrew)
  end
  
  if castable(SB.PurifyingBrew)
    and player.buff(SB.CelestialBrew).down
    and (player.debuff(SB.HeavyStagger).up 
      or ((player.debuff(SB.LightStagger).up or player.debuff(SB.ModerateStagger).up) 
        and (spell(SB.PurifyingBrew).charges == 2 or spell(SB.PurifyingBrew).charges == 1 and spell(SB.PurifyingBrew).recharge < 2))) then
    cast(SB.PurifyingBrew)
  end
  
  if castable(SB.CelestialBrew) and (player.debuff(SB.HeavyStagger).up or player.debuff(SB.ModerateStagger).up) and spell(SB.PurifyingBrew).charges == 0 and player.buff(SB.PurifiedChi).count >= 5 then
    return cast(SB.CelestialBrew)
  end
  
  if toggle('dispell', false) and castable(SB.DetoxDPS) and player.dispellable(SB.DetoxDPS) then
    return cast(SB.DetoxDPS, player)
  end
  
  if toggle('auto_target', false) then
    local nearest_target = enemies.match(function (unit)
      return unit.alive and unit.combat and unit.distance <= 5
    end)
    
    if (not target.exists or target.distance > 5) and nearest_target and nearest_target.name then
      macro('/target ' .. nearest_target.name)
    end
  end
  
  local enemies_around_5 = enemies.around(5)
  
  if player.buff(TG.ChorusofDeadSouls).up then
    if GetActionCooldown(159) == 0 and player.buff(TG.PhaseShift).down then
      UseAction(159)
    end
    
    if GetActionCooldown(157) == 0 and target.enemy and target.distance <= 40 and not player.moving then
      return UseAction(157)
    end
    
    return UseAction(158)
  end
  
  if target.enemy and target.alive and target.distance <= 5 then
    auto_attack()
      
    if toggle('interrupts', false) and target.interrupt(70) then
      if spell(SB.SpearHandStrike).cooldown == 0 then
        return cast(SB.SpearHandStrike, target)
      end
      
      if castable(SB.LegSweep) then
        return cast(SB.LegSweep)
      end

      if target.castable(SB.Paralysis) then
        return cast(SB.Paralysis, target)
      end
    end
    
    if player.channeling(SB.SpinningCrane) then return end
    
    if toggle('cooldowns', false) then
      if castable(SB.Berserking) then
        cast(SB.Berserking)
      end
      
      local start, duration, enable = GetInventoryItemCooldown("player", 13)
      if enable == 1 and start == 0 then
        return macro('/use 13')
      end
      
      start, duration, enable = GetInventoryItemCooldown("player", 14)
      if enable == 1 and start == 0 then
        return macro('/use 14')
      end
      
      if castable(SB.InvokeNiuzao) then
        return cast(SB.InvokeNiuzao, target)
      end
      
      if castable(SB.TouchofDeath) and target.distance <= 5 then
        return cast(SB.TouchofDeath, target)
      end
      
      if castable(SB.WeaponsofOrder) then
        return cast(SB.WeaponsofOrder)
      end
    end
    
    if target.castable(SB.KegSmash) and enemies_around_5 >= 2 then
      return cast(SB.KegSmash, target)
    end
    
    if target.castable(SB.KegSmash) and player.buff(SB.WeaponsofOrder).up then
      return cast(SB.KegSmash, target)
    end
    
    if spell(SB.BlackoutKickBR).cooldown == 0 then
      return cast(SB.BlackoutKickBR, target)
    end
    
    if target.castable(SB.KegSmash) then
      return cast(SB.KegSmash, target)
    end
    
    if castable(SB.RushingJadeWind) and player.buff(SB.RushingJadeWind).down then
      return cast(SB.RushingJadeWind)
    end
    
    if castable(SB.BreathofFire) then
      return cast(SB.BreathofFire)
    end
    
    local energy_to_keg_smash = player.power.energy.actual + player.power.energy.regen * (spell(SB.KegSmash).cooldown + gcd_duration()) 
    
    if castable(SB.ExpelHarm) and player.health.effective < 60 and energy_to_keg_smash >= 55 then
      return cast(SB.ExpelHarm)
    end
    
    if castable(SB.ChiWave) then
      return cast(SB.ChiWave, target)
    end
    
    if castable(SB.SpinningCrane) and enemies_around_5 >= 3 and spell(SB.KegSmash).cooldown > gcd_duration() and energy_to_keg_smash >= 65 then
      return cast(SB.SpinningCrane)
    end
    
    if target.castable(SB.TigerPalm) and spell(SB.KegSmash).cooldown > gcd_duration() and energy_to_keg_smash >= 65 then
      return cast(SB.TigerPalm, target)
    end
    
    if castable(SB.RushingJadeWind) then
      return cast(SB.RushingJadeWind)
    end
  end
end

local function resting()
  if not player.alive then return end
  
  if modifier.lshift and castable(SB.Vivify) then
    return cast(SB.Vivify, player)
  end
  
  if player.health.effective < 50 and castable(SB.Vivify) and not player.moving then
    return cast(SB.Vivify, player)
  end

  if toggle('dispell', false) and castable(SB.DetoxDPS) and player.dispellable(SB.DetoxDPS) then
    return cast(SB.DetoxDPS, player)
  end
end

function interface()
  dark_addon.interface.buttons.add_toggle({
    name = 'dispell',
    label = 'Auto Dispell',
    on = {
      label = 'DSP',
      color = dark_addon.interface.color.green,
      color2 = dark_addon.interface.color.green
    },
    off = {
      label = 'dsp',
      color = dark_addon.interface.color.grey,
      color2 = dark_addon.interface.color.dark_grey
    }
  })
  dark_addon.interface.buttons.add_toggle({
    name = 'auto_target',
    label = 'Auto Target',
    on = {
      label = 'AT',
      color = dark_addon.interface.color.green,
      color2 = dark_addon.interface.color.green
    },
    off = {
      label = 'at',
      color = dark_addon.interface.color.grey,
      color2 = dark_addon.interface.color.dark_grey
    }
  })
end

dark_addon.rotation.register({
  spec = dark_addon.rotation.classes.monk.brewmaster,
  name = 'br_nikopol',
  label = 'Brewmaster by Nikopol',
  gcd = gcd,
  combat = combat,
  resting = resting,
  interface = interface
})
