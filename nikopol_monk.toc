## Interface: 90001
## Title: Monk by Nikopol
## Notes: Rotations for Dark Addon
## LoadOnDemand: 0
## DefaultState: enabled
## Dependencies: dark_addon, HeroLib

spellbook.lua
torghast.lua
brewmaster.lua
mistweaver.lua
windwalker.lua